import { TorrentInfo } from "./TorrentInfo";

export interface TorrentEvent {
    err: string|null,
    torrent_info: TorrentInfo
}