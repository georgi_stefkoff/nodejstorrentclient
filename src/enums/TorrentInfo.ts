export interface TorrentInfo {
    percentage: number,
    remaining_time: number,
    downloaded: number
}