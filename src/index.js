"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TorrentClient_1 = require("./objects/TorrentClient");
var aaa = require('yargs').option('m', {
    alias: 'magnet',
    description: 'Magnet URL'
}).help().argv;
console.log(aaa);
var a = new TorrentClient_1.TorrentClient();
var magnet = 'magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent';
a.addTorrent(magnet, function (torrentHash) {
    a.on(torrentHash, function (torrent) {
        console.log(torrent);
    });
});
