"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var Client = require('webtorrent');
var TorrentClient = /** @class */ (function (_super) {
    __extends(TorrentClient, _super);
    function TorrentClient() {
        var _this = _super.call(this) || this;
        _this._interval = 0;
        _this._client = new Client();
        return _this;
    }
    /**
     *
     * @param {string} hash
     * @param {Error | string | null} err
     * @param {WebTorrent.Torrent | null} torrent
     * @private
     */
    TorrentClient.prototype._sendTorrentInfo = function (hash, err, torrent) {
        if (err) {
            this.emit(hash, err, null);
        }
        if (torrent) {
            this.emit(hash, {
                err: err,
                torrent_info: {
                    percentage: parseFloat((torrent.progress * 100).toFixed(2)),
                    remaining_time: torrent.timeRemaining,
                    downloaded: torrent.downloaded
                }
            });
        }
    };
    /**
     *
     * @param {string} magnetUrl
     * @param {Function | null} cb
     */
    TorrentClient.prototype.addTorrent = function (magnetUrl, cb) {
        var _this = this;
        if (cb === void 0) { cb = null; }
        this._client.add(magnetUrl, { path: './torrent' }, function (torrent) {
            if (cb) {
                cb(torrent.infoHash);
            }
            _this._interval = setInterval(_this._sendTorrentInfo.bind(_this, torrent.infoHash, null, torrent), 500);
            torrent.on('done', function () {
                if (_this._interval) {
                    _this._sendTorrentInfo(torrent.infoHash, null, torrent);
                    clearInterval(_this._interval);
                    torrent.destroy();
                }
            });
            torrent.on('error', function (err) {
                if (_this._interval) {
                    _this._sendTorrentInfo(torrent.infoHash, err, null);
                    clearInterval(_this._interval);
                }
            });
        });
    };
    return TorrentClient;
}(events_1.EventEmitter));
exports.TorrentClient = TorrentClient;
