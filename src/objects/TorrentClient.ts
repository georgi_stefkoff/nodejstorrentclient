import { EventEmitter } from 'events';
import WebTorrent from "webtorrent";
const Client = require('webtorrent');

export class TorrentClient extends EventEmitter{
  private _client: WebTorrent.Instance;
  private _interval: number = 0;
  constructor(){
      super();
      this._client = new Client();
  }

    /**
     *
     * @param {string} hash
     * @param {Error | string | null} err
     * @param {WebTorrent.Torrent | null} torrent
     * @private
     */
  private _sendTorrentInfo(hash: string, err: Error|string|null, torrent: WebTorrent.Torrent|null){
      if(err){
          this.emit(hash, err, null);
      }

      if(torrent){
          this.emit(hash, {
              err: err,
              torrent_info: {
                  percentage: parseFloat((torrent.progress * 100).toFixed(2)),
                  remaining_time: torrent.timeRemaining,
                  downloaded: torrent.downloaded
              }
          })
      }
  }

    /**
     *
     * @param {string} magnetUrl
     * @param {Function | null} cb
     */
  public addTorrent(magnetUrl: string, cb: Function|null = null){
    this._client.add(magnetUrl, { path: './torrent' }, (torrent: WebTorrent.Torrent) => {
        if (cb) {
            cb(torrent.infoHash);
        }

        this._interval = setInterval(this._sendTorrentInfo.bind(this, torrent.infoHash, null, torrent), 500);

        torrent.on('done', () => {
            if(this._interval){
                this._sendTorrentInfo(torrent.infoHash, null, torrent);
                clearInterval(this._interval);
                torrent.destroy();
            }
        });

        torrent.on('error', (err: Error|string) => {
            if(this._interval){
                this._sendTorrentInfo(torrent.infoHash, err, null);
                clearInterval(this._interval);
            }
        });
    });
  }
}